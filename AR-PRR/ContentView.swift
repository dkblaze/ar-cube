//
//  ContentView.swift
//  AR-PRR
//
//  
//

import SwiftUI
import RealityKit
import ARKit

struct ContentView : View {
    var body: some View {
        return ARViewContainer().edgesIgnoringSafeArea(.all)
    }
}

struct ARViewContainer: UIViewRepresentable {
    
    func makeUIView(context: Context) -> ARView {
        
        let arView = ARView(frame: .zero)
        let mesh = MeshResource.generateBox(size: 0.2)
        let material = SimpleMaterial(color: .purple,roughness: 0.5, isMetallic: true)
        
        let modelEntity = ModelEntity(mesh: mesh, materials: [material])
        let anchorEntity = AnchorEntity(plane: .horizontal)
        anchorEntity.name = "Cube"
        anchorEntity.addChild(modelEntity)
        arView.scene.addAnchor(anchorEntity)
        modelEntity.generateCollisionShapes(recursive: true)
        arView.installGestures([.translation,.rotation,.scale], for: modelEntity)
        arView.enableObjectRemoval()
        
        
        
        return arView
        
    }
    
    func updateUIView(_ uiView: ARView, context: Context) {}
    
}
extension ARView{
    
    func enableObjectRemoval(){
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(recognizer:)))
        self.addGestureRecognizer(longPressGesture)
    }
    @objc func handleLongPress(recognizer:UILongPressGestureRecognizer){
        let location  = recognizer.location(in: self)
        
        if let entity = self.entity(at: location){
            if let anchorEntity = entity.anchor,anchorEntity.name  == "Cube"{
                anchorEntity.removeFromParent()
                print("Removed anchor with name :" + anchorEntity.name)
            }
        }
    }
    
    
    
    
    
}




#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
